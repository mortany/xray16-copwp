X-Ray Engine 1.6 + STCoP WP
==========================

Данный репозиторий содержит код для работы мода STCoP Weapon Pack, поддерживает версии начиная с 3.1
* - [x] Обновление оригинальной системы установки аддонов
* - [x] Более стабильный код
* - [x] Полная совместимость с оригинальной системой установки аддонов
* - [ ] Поддержка шейдера для коллиматоров в R1 рендере
